My Local Debian-Linux configuration
=========

the idea was to create a git repository in my linux root partition and then by using a whitelist only include the configuration i really want to keep. that will help me in case when a disaster happens and the good thing is i can keep all the configuration in only one git repository

Whitelist using .gitignore
-----------

the main problem creating a whitelist is that we have not git special files for that except the `.gitignore` file. however we can use that as a mean to somehow create our whitelist. what we need to do is:

1. first we need to ignore everything in the root by `*`
 then we have to include our ignore list `!.gitignore`
2. then we have to include our config directories. for that we have to do it recursively. first we need to add each subdirectory then the contents of the directory
 
file's permission and owner
-----------
git doesn't keep track of permissions and owners for you! to solve this you can use [git-cache-meta script](http://twitter.github.com/bootstrap/) to keep and restore the permissions. to keep file permission you can use `--restore` or `--stdout` argument:

    # this will save file permission into .git_cache_meta file
    git-cache-meta --store

to restore the permission after a pull request, you can use `--apply` argument:

    # after a pull request
    git-cache-meta --apply


Version
-

0.1

References
-----------

this work is done thanks to these magnificent people:

* [andris9] https://gist.github.com/andris9/1978266
* [vonc@stackoverflow] http://stackoverflow.com/a/3208143